package com.betskey.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import com.betskey.R
import com.betskey.constants.AppConstants
import com.betskey.constants.Config
import com.betskey.constants.IntentKeys
import com.betskey.constants.PreferenceKeys
import com.betskey.dataModels.HighlightDataModel
import com.betskey.dataModels.LiabilityDataModel
import com.betskey.databinding.ActivityDashboardBinding
import com.betskey.fragments.HomeFragment
import com.betskey.fragments.InPlayFragment
import com.betskey.fragments.MultiMarketFragment
import com.betskey.fragments.MyAccountFragment
import com.betskey.responseModels.Common
import com.betskey.retrofit.RetrofitApiClient
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import microsoft.aspnet.signalr.client.Platform
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent
import microsoft.aspnet.signalr.client.hubs.HubConnection
import microsoft.aspnet.signalr.client.hubs.HubProxy
import microsoft.aspnet.signalr.client.transport.ClientTransport
import microsoft.aspnet.signalr.client.transport.LongPollingTransport
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat

class DashboardActivity : AppCompatActivity() {

    lateinit var selectedFragment: Fragment
    val TAG: String = "DashboardActivity"
    lateinit var binding: ActivityDashboardBinding
    lateinit var connection: HubConnection
    lateinit var hub: HubProxy
    var transport: ClientTransport? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    override fun onResume() {
        super.onResume()
        getBalanceAndLiability()
        AppConstants.matchClickedMarket = false
        AppConstants.liabilityClickedMarket = false
    }

    companion object {
        var inPlayList = ArrayList<HighlightDataModel>()
        lateinit var navigationMenu: BottomNavigationView
        lateinit var tvBalance: TextView
        lateinit var tvLiability: TextView
        lateinit var tvBalanceAcc: TextView
        lateinit var tvLiabilityAcc: TextView
        var closeApp = true
        var liabilityList = ArrayList<LiabilityDataModel>()
        var decimalFormat0_00 = DecimalFormat(AppConstants.format0_00)
        var decimalFormat0_0 = DecimalFormat(AppConstants.format0_0)
        var isMatchClicked = false
        var marketIds: String = ""
    }

    private fun init() {
        Log.e(TAG, "AuthToken: " + Config.getSharedPreferences(this, PreferenceKeys.AuthToken))
        Log.e(TAG, "BetToken: " + Config.getSharedPreferences(this, PreferenceKeys.BetToken))
        Config.setUserLoggedIn(this, PreferenceKeys.userLoggedIn, true)
        binding.scrollingNews.text = Config.getSharedPreferences(this, PreferenceKeys.newsTitle)
        binding.scrollingNews.isSelected = true
        binding.tvUsername.text = Config.getSharedPreferences(this, PreferenceKeys.Username)

        setUpBottomNavigation()
        navigationMenu = binding.navigationView
        tvBalance = binding.tvBalance
        tvLiability = binding.tvLiability
        tvBalanceAcc = binding.tvBalanceAcc
        tvLiabilityAcc = binding.tvLiabilityAcc
        binding.tvLiability.setOnClickListener {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                binding.tvLiability, "liability_transition"
            )
            val intent =
                Intent(this@DashboardActivity, LiabilityDetailActivity::class.java)
            intent.putExtra(IntentKeys.liabilityData, liabilityList)
            startActivity(intent, options.toBundle())
        }
    }

    private fun setUpBottomNavigation() {
        loadFragment(HomeFragment())
        binding.navigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    loadFragment(HomeFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_in_play -> {
                    loadFragment(InPlayFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_market -> {
                    isMatchClicked = false
                    navigationMenu.menu.getItem(2).title = "Multi-Market"
                    loadFragment(MultiMarketFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_my_account -> {
                    loadFragment(MyAccountFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                else -> supportFragmentManager.popBackStack()
            }
            false
        }
    }

    private fun loadFragment(fragment: Fragment) {
        if (fragment is MyAccountFragment) {
            binding.llMainToolbar.visibility = View.GONE
            binding.llAccountToolbar.visibility = View.VISIBLE
            binding.tvUserName.text =
                Config.getSharedPreferences(this, PreferenceKeys.Username)
            binding.tvBalanceAcc.text =
                Config.getSharedPreferences(this, PreferenceKeys.balance)
            binding.tvLiabilityAcc.text =
                Config.getSharedPreferences(this, PreferenceKeys.liability)
            /*binding.ivLogout.setOnClickListener {
                Config.showLogoutConfirmationDialog(
                    this, this
                )
            }*/
        } else {
            binding.llMainToolbar.visibility = View.VISIBLE
            binding.llAccountToolbar.visibility = View.GONE
        }
        selectedFragment = fragment
        // load fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
//        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        /* if (selectedFragment is HomeFragment)
             super.onBackPressed()
         else {
             loadFragment(HomeFragment())
             binding.navigationView.menu.getItem(0).isChecked = true
         }*/
        if (closeApp) {
            Config.showConfirmationDialog(this, object : Config.OkButtonClicklistner {
                override fun OkButtonClick() {
                    finish()
                }
            })
        } else {
            closeApp = true
            loadFragment(HomeFragment())
            binding.navigationView.menu.getItem(0).isChecked = true
        }
    }

    private fun getBalanceAndLiability() {
        try {
            if (Config.isInternetAvailable(this@DashboardActivity)) {
                val call: Call<Common> = RetrofitApiClient.getClient.getBalanceAndLiability(
                    Config.getSharedPreferences(this@DashboardActivity, PreferenceKeys.AuthToken)
                )
                call.enqueue(object : Callback<Common> {
                    override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                        val common: Common? = response?.body()
                        Log.e(TAG, "getBalanceAndLiability: " + Gson().toJson(common))
                        if (response != null && response.isSuccessful) {
                            when (common?.status?.code) {
                                0 -> {
                                    Config.saveBalanceAndLiability(
                                        common.RunningBalance,
                                        common.Liability, this@DashboardActivity
                                    )
                                }
                                /*401 -> {
                                    val realm = Realm.getDefaultInstance()
                                    realm.executeTransaction { realm -> realm.deleteAll() }
                                    Config.clearAllPreferences(this@DashboardActivity)
                                    startActivity(
                                        Intent(
                                            this@DashboardActivity,
                                            LoginActivity::class.java
                                        )
                                    )
                                    finish()
                                }*/
                                else -> Config.toast(
                                    this@DashboardActivity,
                                    common?.status?.returnMessage
                                )
                            }
                        }
                    }

                    override fun onFailure(call: Call<Common>?, t: Throwable?) {
                        Log.e(TAG, "getBalanceAndLiability: " + t.toString())
                    }
                })
            }
        } catch (e: Exception) {
            Log.e(TAG, "" + e)
        }
    }

    private fun connectSignalR() {
        Platform.loadPlatformComponent(AndroidPlatformComponent())
        connection = HubConnection("192.168.1.100"); //socket server url
        hub = connection.createHubProxy("chatHub"); // case insensitivity
        hub.subscribe(this);
        transport = LongPollingTransport(connection.getLogger());

        connection.start(transport)
        // fetch data from server
        connection.received { json ->
            runOnUiThread {
                val jsonObject = json.asJsonObject
                Log.e("<Debug>", "response = $jsonObject")
            }
        }
    }
}