package com.betskey.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.betskey.databinding.ActivityTermsnConditionsBinding

class TermsnConditionsActivity : AppCompatActivity() {

    val TAG: String = "TermsnConditionsActivity"
    lateinit var binding: ActivityTermsnConditionsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTermsnConditionsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.btnAccept.setOnClickListener {
            val btnAccept = Pair.create<View, String>(binding.btnAccept, "login_transition")
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnAccept)
            startActivity(Intent(this, LoginActivity::class.java), options.toBundle())
            finish()
        }
    }
}