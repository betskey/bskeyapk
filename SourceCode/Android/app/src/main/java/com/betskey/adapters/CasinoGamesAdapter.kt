package com.betskey.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.responseModels.CasinoGames
import com.bumptech.glide.Glide

class CasinoGamesAdapter(private val items: ArrayList<CasinoGames>?, val context: Context) :
    RecyclerView.Adapter<CasinoGamesAdapter.MyViewHolder>() {

    val TAG: String = "HorizontalAdapter"
    var mClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.horizontal_view_layout, parent, false)
        return MyViewHolder(view)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llItem: LinearLayout = itemView.findViewById(R.id.ll_item)
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val dataModel: CasinoGames = items!![position]
        holder.tvName.text = dataModel.Name
        var imageUrl = dataModel.Image
        imageUrl = imageUrl.replace("http:", "https:")
        Glide.with(context)
//            .load(UrlConstants.webUrl + dataModel.Image)
//            .load(dataModel.Image)
            .load(imageUrl)
            .into(holder.ivImage)
        holder.llItem.setOnClickListener {
            mClickListener?.onItemClick(it, position)
        }
    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        mClickListener = itemClickListener
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}