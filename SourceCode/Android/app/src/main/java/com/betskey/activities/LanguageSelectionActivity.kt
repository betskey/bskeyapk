package com.betskey.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.betskey.R
import com.betskey.constants.AppConstants
import com.betskey.constants.Config
import com.betskey.constants.PreferenceKeys
import com.betskey.databinding.ActivityLangSelectBinding


class LanguageSelectionActivity : AppCompatActivity() {

    var TAG: String = "SplashActivity"
    private lateinit var binding: ActivityLangSelectBinding
    var langSelected:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLangSelectBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.btnGetStart.setOnClickListener {
            if (langSelected) {
                val btnStart =
                    Pair.create<View, String>(findViewById(R.id.btn_get_start), "tnc_transition")
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnStart)
                startActivity(Intent(this@LanguageSelectionActivity, TermsnConditionsActivity::class.java))
                finishAffinity()
            } else{
                Config.toast(this, getString(R.string.please_select_one_lang))
            }
        }
        binding.tvEnglish.setOnClickListener {
            langSelected = true
            binding.tvEnglish.setBackgroundColor(resources.getColor(R.color.colorWhite))
            binding.tvEnglish.setTextColor(resources.getColor(R.color.bgCard))
            binding.tvHindi.background = null
            binding.tvHindi.setTextColor(resources.getColor(R.color.colorWhite))
            Config.saveSharedPreferences(this@LanguageSelectionActivity, PreferenceKeys.Language,AppConstants.en)
        }
        binding.tvHindi.setOnClickListener {
            langSelected = true
            binding.tvEnglish.background = null
            binding.tvEnglish.setTextColor(resources.getColor(R.color.colorWhite))
            binding.tvHindi.setBackgroundColor(resources.getColor(R.color.colorWhite))
            binding.tvHindi.setTextColor(resources.getColor(R.color.bgCard))
            Config.saveSharedPreferences(this@LanguageSelectionActivity, PreferenceKeys.Language,AppConstants.hi)
        }
    }
}