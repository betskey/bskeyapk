package com.betskey.fragments

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.activities.CasinoGamesActivity
import com.betskey.activities.DashboardActivity
import com.betskey.activities.LoginActivity
import com.betskey.adapters.CasinoGamesAdapter
import com.betskey.adapters.MatchViewPagerAdapter
import com.betskey.constants.Config
import com.betskey.constants.IntentKeys
import com.betskey.constants.PreferenceKeys
import com.betskey.dataModels.HighlightDataModel
import com.betskey.databinding.FragmentHomeBinding
import com.betskey.responseModels.CasinoGames
import com.betskey.responseModels.Common
import com.betskey.responseModels.Highlights
import com.betskey.responseModels.Match
import com.betskey.retrofit.RetrofitApiClient
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeFragment : Fragment(), CasinoGamesAdapter.ItemClickListener {

    val TAG: String = "HomeFragment"
    lateinit var fragmentFirstBinding: FragmentHomeBinding
    val mContext: FragmentActivity? = activity
    var casinoGamesList = ArrayList<CasinoGames>()
    var highlightsList = ArrayList<Highlights>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentFirstBinding = FragmentHomeBinding.inflate(inflater, container, false)
        val view: View = fragmentFirstBinding.root
        init()
        return view
    }

    private fun init() {
        fragmentFirstBinding.refresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                requireContext(), R.color.bgColor
            )
        )
        fragmentFirstBinding.refresh.setColorSchemeColors(Color.WHITE)
        DashboardActivity.inPlayList.clear()

        fragmentFirstBinding.btnReloadData.setOnClickListener {
            getDashboardDetail()
        }

        fragmentFirstBinding.refresh.setOnRefreshListener {
            getDashboardDetail()
            fragmentFirstBinding.refresh.isRefreshing = false
        }

        getDashboardDetail()
    }

    private fun getDashboardDetail() {
        casinoGamesList.clear()
        highlightsList.clear()
        try {
            if (Config.isInternetAvailable(requireContext())) {
                fragmentFirstBinding.llData.visibility = View.VISIBLE
                fragmentFirstBinding.llNoInternet.visibility = View.GONE
                Config.showSmallProgressDialog(requireContext())
                val call: Call<Common> = RetrofitApiClient.getClient.getDashboardDetail(
                    Config.getSharedPreferences(requireContext(), PreferenceKeys.AuthToken)
                )
                call.enqueue(object : Callback<Common> {
                    override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                        val common: Common? = response?.body()
                        Log.e(TAG, "getDashboardDetail: " + Gson().toJson(common))
                        Config.hideSmallProgressDialog()
                        if (response != null && response.isSuccessful) {
                            when (common?.status?.code) {
                                0 -> {
//                                    casinoGamesList = common.DashboardData.CasinoGames
                                    casinoGamesList = common.VeronicaGameList.GameList
                                    highlightsList = common.DashboardData.Highlights
                                    setHorizontalList()
                                    setupViewPager()
                                    setInPLayList()
                                }
                                401 -> {
                                    val realm = Realm.getDefaultInstance()
                                    realm.executeTransaction { realm -> realm.deleteAll() }
                                    Config.clearAllPreferences(requireContext())
                                    startActivity(
                                        Intent(
                                            requireContext(),
                                            LoginActivity::class.java
                                        )
                                    )
                                    activity!!.finish()
                                }
                                else -> Config.toast(
                                    requireContext(),
                                    common?.status?.returnMessage
                                )
                            }
                        } else {
                            val realm = Realm.getDefaultInstance()
                            realm.executeTransaction { realm -> realm.deleteAll() }
                            Config.clearAllPreferences(requireContext())
                            startActivity(
                                Intent(
                                    requireContext(),
                                    LoginActivity::class.java
                                )
                            )
                            activity!!.finish()
                        }
                    }

                    override fun onFailure(call: Call<Common>?, t: Throwable?) {
                        Config.hideSmallProgressDialog()
                        Log.e(TAG, "getDashboardDetail: " + t.toString())
                    }
                })
            } else {
                Config.hideSmallProgressDialog()
                fragmentFirstBinding.llData.visibility = View.GONE
                fragmentFirstBinding.llNoInternet.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            Config.toast(requireContext(), "" + e)
            Log.e(TAG, "" + e)
        }
    }

    private fun setInPLayList() {
        if (highlightsList.size > 0) {
            for (i in 0 until highlightsList.size) {
                val matchList = ArrayList<Match>()
                matchList.addAll(highlightsList[i].Match)
                val highlights = HighlightDataModel()
                highlights.SportName = highlightsList[i].SportName
                highlights.SportId = highlightsList[i].SportId
                if (matchList.size > 0) {
                    for (i in 0 until matchList.size) {
                        if (matchList[i].IsInPlay)
                            highlights.Match.add(matchList[i])
                    }
                }
                if (highlights.Match.size > 0)
                    DashboardActivity.inPlayList.add(highlights)
            }
        }
    }

    private fun setHorizontalList() {
        fragmentFirstBinding.recyclerHomeList.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.HORIZONTAL, false
        )
        val adapter = CasinoGamesAdapter(casinoGamesList, activity!!.applicationContext)
        fragmentFirstBinding.recyclerHomeList.adapter = adapter
        adapter.setClickListener(this)
    }

    private fun setupViewPager() {
        if (highlightsList.size > 0) {
            fragmentFirstBinding.viewpager.visibility = View.VISIBLE
            fragmentFirstBinding.tvNoData.visibility = View.GONE
            val viewPagerAdapter = MatchViewPagerAdapter(activity!!.supportFragmentManager)
            val fragments: ArrayList<Fragment> = ArrayList()
            //dynamically sets the list in viewpager
            for (i in 0 until highlightsList.size) {
                val b = Bundle()
                b.putSerializable(IntentKeys.matchListData, highlightsList[i])
                fragments.add(instantiate(requireContext(), MatchListFragment::class.java.name, b))
                viewPagerAdapter.add(fragments, highlightsList[i].SportName, highlightsList[i])
            }
            viewPagerAdapter.notifyDataSetChanged()
            fragmentFirstBinding.viewpager.adapter = viewPagerAdapter
            fragmentFirstBinding.tabLayout.setupWithViewPager(fragmentFirstBinding.viewpager, true)
            setUpTabIcons()
        } else {
            fragmentFirstBinding.viewpager.visibility = View.GONE
            fragmentFirstBinding.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun setUpTabIcons() {
        try {
            for (i in 0 until highlightsList.size) {
                var gameName = highlightsList[i].SportName.toLowerCase()
                if (gameName.contains(" "))
                    gameName = gameName.replace(" ", "")
                val id: Int = context?.resources!!
                    .getIdentifier(gameName, "drawable", requireContext().packageName)
                if (id != 0) {
                    fragmentFirstBinding.tabLayout.getTabAt(i)?.setIcon(id)
                    if (i == 0) {
                        fragmentFirstBinding.tabLayout.getTabAt(i)?.icon?.setColorFilter(
                            Color.WHITE,
                            PorterDuff.Mode.SRC_IN
                        )
                    } else {
                        fragmentFirstBinding.tabLayout.getTabAt(i)?.icon?.setColorFilter(
                            Color.BLACK,
                            PorterDuff.Mode.SRC_IN
                        )
                    }
                }
            }

            fragmentFirstBinding.tabLayout.setOnTabSelectedListener(
                object : TabLayout.ViewPagerOnTabSelectedListener(fragmentFirstBinding.viewpager) {
                    override fun onTabSelected(tab: TabLayout.Tab) {
                        super.onTabSelected(tab)
                        val tabIconColor =
                            ContextCompat.getColor(context!!, R.color.colorWhite)
                        if (tab.icon != null)
                            tab.icon!!.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN)
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {
                        super.onTabUnselected(tab)
                        val tabIconColor =
                            ContextCompat.getColor(context!!, R.color.colorBlack)
                        if (tab.icon != null)
                            tab.icon!!.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN)
                    }
                }
            )
        } catch (e: Exception) {
            Config.toast(requireContext(), "" + e)
            Log.e(TAG, "" + e)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onItemClick(view: View?, position: Int) {
        val casinoModel = casinoGamesList[position]
        val intent = Intent(requireContext(), CasinoGamesActivity::class.java)
        intent.putExtra(IntentKeys.casinoGame, casinoModel)
        startActivity(intent)
    }
}