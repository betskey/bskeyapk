package com.betskey.dataModels

class RoundDataModel {
    var RoundSummaryID: Int = 0
    var OpenNumber: String = ""
    var CloseNumber: String = ""
    var Winner: String = ""
    var GameID: Int = 0
    var Status: Int = 0
    var IsRoundOver: Boolean = false
}