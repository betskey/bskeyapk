package com.betskey.constants

object UrlConstants {

    val webUrl = "https://dev.betskey.com/"
//    val URLMobileApi = " https://dev.betskey.com/Api/MobileApi/"
//    val URLMobileMarketApi = " https://dev.betskey.com/Api/MobileMarketApi/"

//    val webUrl = "https://devapi.betskey.com/"
    val URLMobileApi = " https://devapi.betskey.com/Api/MobileApi/"
    val URLMobileMarketApi = " https://devapi.betskey.com/Api/MobileMarketApi/"
}