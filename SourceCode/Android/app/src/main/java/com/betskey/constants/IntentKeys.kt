package com.betskey.constants

object IntentKeys {

    const val casinoGame: String = "casinoGame"
    const val matched: String = "matched"
    const val unMatched: String = "unMatched"
    const val liabilityData: String = "liabilityData"
    const val username: String = "username"
    const val matchListData: String = "matchListData"
    const val data: String = "data"
    const val marketID: String = "marketID"
    const val matchName: String = "matchName"
    const val siteTypeID: String = "siteTypeID"
    const val sportName: String = "sportName"
    const val description: String = "description"
}