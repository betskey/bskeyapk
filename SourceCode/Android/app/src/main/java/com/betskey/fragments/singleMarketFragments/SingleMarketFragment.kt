package com.betskey.fragments.singleMarketFragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.activities.LoginActivity
import com.betskey.activities.SingleMarketActivity
import com.betskey.adapters.SingleMarketAdapter
import com.betskey.adapters.SingleMatchAdapter
import com.betskey.adapters.SingleRunnerAdapter
import com.betskey.constants.AppConstants
import com.betskey.constants.Config
import com.betskey.constants.JsonKeys
import com.betskey.constants.PreferenceKeys
import com.betskey.dataModels.DownlineModel
import com.betskey.dataModels.MarketDataModel
import com.betskey.dataModels.MarketModel
import com.betskey.dataModels.RunnerModel
import com.betskey.databinding.LayoutSocketDataBinding
import com.betskey.fragments.multiMarketFragments.SocketMarketFragment
import com.betskey.responseModels.Common
import com.betskey.retrofit.RetrofitApiClient
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.Realm
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SingleMarketFragment : Fragment() {

    val TAG: String = "SingleMarketFragment"
    lateinit var fragmentBinding: LayoutSocketDataBinding
    var marketIds: String = ""
    lateinit var socketMatchAdp: SingleMatchAdapter
    var centralIds: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentBinding = LayoutSocketDataBinding.inflate(inflater, container, false)
        fragmentBinding.lifecycleOwner = this.viewLifecycleOwner
        val view: View = fragmentBinding.root

        init()

        return view
    }

    private fun init() {
        marketIds = SingleMarketActivity.marketIds
        socketMatchAdp = SingleMatchAdapter()

        fragmentBinding.refresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(requireContext(), R.color.bgColor)
        )
        fragmentBinding.refresh.setColorSchemeColors(Color.WHITE)
        fragmentBinding.refresh.setOnRefreshListener {
            if (SingleMarketActivity.isActive) {
                SingleMarketActivity.webSocket.close(1000, null)
                SingleMarketActivity.isActive = false
            }
            callMarketDetail()
            fragmentBinding.refresh.isRefreshing = false
        }


        callMarketDetail()
    }

    private fun updateUI() {
        if (marketList.size > 0) {
            fragmentBinding.recyclerMarketList.visibility = View.VISIBLE
            fragmentBinding.tvNoData.visibility = View.GONE
        } else {
            fragmentBinding.recyclerMarketList.visibility = View.GONE
            fragmentBinding.tvNoData.visibility = View.VISIBLE
        }
    }

    private fun callMarketDetail() {
        marketList.clear()
        val jsonObject = JsonObject()
        jsonObject.addProperty(JsonKeys.MarketIds, marketIds)
        Log.e(TAG, "jsonObject: $jsonObject")
        if (Config.isInternetAvailable(requireContext())) {
            Config.showSmallProgressDialog(requireContext())
            val call: Call<Common> = RetrofitApiClient.getClient.getMarketDetail(
                Config.getSharedPreferences(requireContext(), PreferenceKeys.AuthToken), jsonObject
            )
            call.enqueue(object : Callback<Common> {
                override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                    Log.e(TAG, "getMarketDetail: " + Gson().toJson(response?.body()))
                    val common: Common? = response?.body()
                    if (response != null && response.isSuccessful) {
                        Config.hideSmallProgressDialog()
                        when (common?.status?.code) {
                            0 -> {
                                val centralIDList = ArrayList<Int>()
                                for (i in 0 until common.MarketData.size) {
                                    val model: MarketDataModel = common.MarketData[i]
                                    model.isFancyMarket = false
                                    marketList.add(model)
                                    for (j in 0 until common.MarketData[i].Market.size) {
                                        centralIDList.add(common.MarketData[i].Market[j].CentralId)
                                    }
                                }
                                for (i in 0 until common.FancyMarketData.size) {
                                    val model: MarketDataModel = common.FancyMarketData[i]
                                    model.isFancyMarket = true
                                    marketList.add(model)
                                    for (j in 0 until common.FancyMarketData[i].Market.size) {
                                        centralIDList.add(common.FancyMarketData[i].Market[j].CentralId)
                                    }
                                }

                                centralIds = TextUtils.join(",", centralIDList)
                                if (centralIds.isNotEmpty()) {
                                    SingleMarketActivity.isActive = true
                                    connectWebSocket()
                                }
                                DownLineData = common.Downline
                                socketMatchAdp.submitList(marketList)
                                fragmentBinding.recyclerMarketList.adapter = socketMatchAdp
                                updateUI()
                            }
                            401 -> {
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction { realm -> realm.deleteAll() }
                                Config.clearAllPreferences(requireContext())
                                startActivity(Intent(requireContext(), LoginActivity::class.java))
                                activity!!.finish()
                            }
                            else -> {
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction { realm -> realm.deleteAll() }
                                Config.clearAllPreferences(requireContext())
                                startActivity(Intent(requireContext(), LoginActivity::class.java))
                                activity!!.finish()
                            }
                        }
                    } else {
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction { realm -> realm.deleteAll() }
                        Config.clearAllPreferences(requireContext())
                        startActivity(Intent(requireContext(), LoginActivity::class.java))
                        activity!!.finish()
                    }
                }

                override fun onFailure(call: Call<Common>?, t: Throwable?) {
                    Config.hideSmallProgressDialog()
                    Log.e(TAG, "getMarketDetail: " + t.toString())
                }
            })
        } else {
            Config.hideSmallProgressDialog()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun updateSocketData(data: JsonObject, messageType: String) {
        isSocketOn = true
        try {
            if (messageType == JsonKeys.score) {
                for (i in 0 until marketList.size) {
                    val match = marketList[i]
                    for (k in 0 until marketList[i].Market.size) {
                        val marketModel = marketList[i].Market[k]
                        if (data.get(JsonKeys.centralId).asString == "" + marketModel.CentralId) {
                            val score =
                                data.get(JsonKeys.data).asJsonObject.get(JsonKeys.score).asJsonObject
                            if (score.has(JsonKeys.required_runs))
                                match.inningType.set(getString(R.string.innings_2))
                            else
                                match.inningType.set(getString(R.string.innings_1))
                            if (score.get(JsonKeys.status).asString.equals(
                                    JsonKeys.scheduled, true
                                )
                            ) {
                                match.inPlay.set(false)
                            } else if (score.get(JsonKeys.status).asString.equals(
                                    JsonKeys.in_play, true
                                )
                            ) {
                                val teamA = score.get(JsonKeys.team_a).asJsonObject
                                val teamB = score.get(JsonKeys.team_b).asJsonObject
                                match.inPlay.set(true)
                                //set batting team data
                                if (score.get(JsonKeys.batting_team).asString.equals(
                                        teamA.get(JsonKeys.name).asString,
                                        true
                                    )
                                ) {
                                    match.battingTeam.set(teamA.get(JsonKeys.name).asString)
//                                    match.inningType.set("Innings 1")
                                    val inning1 = teamA.get(JsonKeys.innings_1).asJsonObject
                                    val rwo: String =
                                        inning1.get(JsonKeys.runs).asString + "/" +
                                                inning1.get(JsonKeys.wickets).asString +
                                                "  (" +
                                                inning1.get(JsonKeys.overs).asString + ")"
                                    match.runWicketOverBatT.set(rwo)
                                } else if (score.get(JsonKeys.batting_team).asString.equals(
                                        teamB.get(JsonKeys.name).asString,
                                        true
                                    )
                                ) {
                                    match.battingTeam.set(teamB.get(JsonKeys.name).asString)
//                                    match.inningType.set("Innings 1")
                                    val inning1 = teamB.get(JsonKeys.innings_1).asJsonObject
                                    val rwo: String =
                                        inning1.get(JsonKeys.runs).asString + "/" +
                                                inning1.get(JsonKeys.wickets).asString + "  (" +
                                                inning1.get(JsonKeys.overs).asString + ")"
                                    match.runWicketOverBatT.set(rwo)
                                }
                                //set bowling team data
                                if (score.get(JsonKeys.bowling_team).asString.equals(
                                        teamA.get(JsonKeys.name).asString,
                                        true
                                    )
                                ) {
                                    match.bowlingTeam.set(teamA.get(JsonKeys.name).asString)
//                                    match.inningType.set("Innings 1")
                                    val inning1 = teamA.get(JsonKeys.innings_1).asJsonObject
                                    val rwo: String =
                                        inning1.get(JsonKeys.runs).asString + "/" +
                                                inning1.get(JsonKeys.wickets).asString + "  (" +
                                                inning1.get(JsonKeys.overs).asString + ")"
                                    match.runWicketOverBowlT.set(rwo)
                                } else if (score.get(JsonKeys.bowling_team).asString.equals(
                                        teamB.get(JsonKeys.name).asString,
                                        true
                                    )
                                ) {
                                    match.bowlingTeam.set(teamB.get(JsonKeys.name).asString)
//                                    match.inningType.set("Innings 1")
                                    val inning1 = teamB.get(JsonKeys.innings_1).asJsonObject
                                    val rwo: String =
                                        inning1.get(JsonKeys.runs).asString + "/" +
                                                inning1.get(JsonKeys.wickets).asString + "  (" +
                                                inning1.get(JsonKeys.overs).asString + ")"
                                    match.runWicketOverBowlT.set(rwo)
                                }
                                match.tossStatus.set(score.get(JsonKeys.toss).asString)
                                break
                            }
                        }
                    }
                }
            } else if (isSocketOn && context != null) {
                for (j in 0 until marketList.size) {
                    for (k in 0 until marketList[j].Market.size) {
                        val marketModel = marketList[j].Market[k]
//                        marketModel.socketTime.set(SingleMarketActivity.socketTime)
                        when (marketModel.MarketStatus) {
                            3 -> {
                                marketModel.suspendedText.set(getString(R.string.suspended))
                                marketModel.showSuspended.set(true)
                                for (a in 0 until marketModel.Runner.size) {
                                    marketModel.Runner[a].keyboardOpen.set(false)
                                }
                            }
                            9 -> {
                                /* marketModel.suspendedText.set(getString(R.string.ball_started))
                                 marketModel.showSuspended.set(true)
                                 for (a in 0 until marketModel.Runner.size) {
                                     marketModel.Runner[a].keyboardOpen.set(false)
                                 }*/
                            }
                            4 -> {
                                marketModel.suspendedText.set(getString(R.string.closed))
                                marketModel.showSuspended.set(true)
                                for (a in 0 until marketModel.Runner.size) {
                                    marketModel.Runner[a].keyboardOpen.set(false)
                                }
                            }
                        }
                        if (marketModel.CentralId == data.get(JsonKeys.appMarketID).asInt) {
                            socketModel = data
                            val time = SimpleDateFormat(AppConstants.ddMMMyyyyHHmmssSSS).format(
                                SimpleDateFormat(AppConstants.yyyyMMddTHHmmSS).parse(
                                    Config.convertIntoLocal(
                                        Config.stringToDate(
                                            socketModel.get(JsonKeys.appGetRecordTime).asString,
                                            AppConstants.yyyyMMddTHHmmSS
                                        )!!,
                                        AppConstants.yyyyMMddTHHmmSS
                                    )
                                )!!
                            )
//                            marketModel.socketTime.set("Time : $time")
                            /*marketStatus: 1-Open, 9-Ball Started,4-Closed, 3-Suspended*/
                            when (socketModel.get(JsonKeys.appMarketStatus).asString) {
                                "1" -> {
                                    marketModel.showSuspended.set(false)
                                    if (!socketModel.get(JsonKeys.appRate).isJsonNull) {
                                        //time when rate update
                                        marketModel.socketTime.set(SingleMarketActivity.socketTime)
                                        socketAppRateArray =
                                            socketModel.get(JsonKeys.appRate).asJsonArray
                                        for (a in 0 until marketModel.Runner.size) {
                                            var isBackDone = false
                                            var isLayDone = false
                                            val runnerModel = marketModel.Runner[a]
                                            val runnable = Runnable {
                                                runnerModel.changeBackBgColor.set(
                                                    false
                                                )
                                                runnerModel.changeLayBgColor.set(
                                                    false
                                                )
                                            }
                                            if (marketModel.MarketType.equals(
                                                    AppConstants.Bookmakers,
                                                    true
                                                )
                                            ) {
                                                runnerModel.backRate.set("-")
                                                runnerModel.backBFVolume.set("---")
                                                runnerModel.layRate.set("-")
                                                runnerModel.layBFVolume.set("---")
                                            }
                                            for (x in 0 until socketAppRateArray.size()) {
                                                if (isBackDone && isLayDone)
                                                    break
                                                else if (socketAppRateArray[x].asJsonObject.get(
                                                        JsonKeys.appIsBack
                                                    ).asBoolean
                                                ) {
                                                    if (!isBackDone) {
                                                        if (runnerModel.BfRunnerId == socketAppRateArray[x]
                                                                .asJsonObject.get(JsonKeys.appSelectionID_BF).asLong
                                                        ) {
                                                            var appRate =
                                                                "" + socketAppRateArray[x].asJsonObject.get(
                                                                    JsonKeys.appRate
                                                                )
                                                            appRate =
                                                                appRate.replace("\"", "")
                                                            /*if (!appRate.equals(
                                                                    runnerModel.backRate.get(),
                                                                    true)) {
                                                                runnerModel.changeBackBgColor.set(
                                                                    true
                                                                )
                                                                val handler = Handler()
                                                                handler.postDelayed(runnable, 50)
                                                            }*/
                                                            runnerModel.backRate.set("" + appRate)

                                                            //Back rate, For markets other than Fancy
                                                            if (messageType != JsonKeys.fancy ||
                                                                marketModel.MarketType.equals(
                                                                    AppConstants.Bookmakers,
                                                                    true
                                                                ) || marketModel.MarketType.equals(
                                                                    AppConstants.ManualOdds,
                                                                    true
                                                                )
                                                            ) {
                                                                runnerModel.backBFVolume.set(
                                                                    "" + Math.round(
                                                                        socketAppRateArray[x].asJsonObject.get(
                                                                            JsonKeys.appBFVolume
                                                                        ).asDouble
                                                                    )
                                                                )
                                                            } else if (messageType == JsonKeys.fancy) {
                                                                runnerModel.backBFVolume.set(
                                                                    "" + socketAppRateArray[x].asJsonObject.get(
                                                                        JsonKeys.appPoint
                                                                    ).asInt
                                                                )
                                                            } else {
                                                                runnerModel.backBFVolume.set(
                                                                    "" + Math.round(
                                                                        socketAppRateArray[x].asJsonObject.get(
                                                                            JsonKeys.appBFVolume
                                                                        ).asDouble
                                                                    )
                                                                )
                                                            }
                                                            isBackDone = true
                                                        } else {
                                                            if (marketModel.MarketType.equals(
                                                                    AppConstants.Bookmakers,
                                                                    true
                                                                )
                                                            ) {
                                                                runnerModel.backRate.set("-")
                                                                runnerModel.backBFVolume.set("---")
                                                                runnerModel.layRate.set("-")
                                                                runnerModel.layBFVolume.set("---")
                                                            }
                                                        }
                                                    }
                                                } else if (!isLayDone) {
                                                    if (runnerModel.BfRunnerId == socketAppRateArray[x].asJsonObject.get(
                                                            JsonKeys.appSelectionID_BF
                                                        ).asLong
                                                    ) {
                                                        var appRate =
                                                            "" + socketAppRateArray[x].asJsonObject.get(
                                                                JsonKeys.appRate
                                                            )
                                                        appRate = appRate.replace("\"", "")
                                                        /*if (!appRate.equals(
                                                                runnerModel.layRate.get(),
                                                                true)) {
                                                            runnerModel.changeLayBgColor.set(
                                                                true
                                                            )
                                                            val handler = Handler()
                                                            handler.postDelayed(runnable, 100)
                                                        }*/
                                                        runnerModel.layRate.set("" + appRate)

                                                        //Lay rate, For markets other than Fancy
                                                        if (messageType != JsonKeys.fancy || marketModel.MarketType.equals(
                                                                AppConstants.Bookmakers,
                                                                true
                                                            ) || marketModel.MarketType.equals(
                                                                AppConstants.ManualOdds,
                                                                true
                                                            )
                                                        ) {
                                                            runnerModel.layBFVolume.set(
                                                                "" + Math.round(
                                                                    socketAppRateArray[x].asJsonObject.get(
                                                                        JsonKeys.appBFVolume
                                                                    ).asDouble
                                                                )
                                                            )
                                                        } else if (messageType == JsonKeys.fancy) {
                                                            runnerModel.layBFVolume.set(
                                                                "" + socketAppRateArray[x].asJsonObject.get(
                                                                    JsonKeys.appPoint
                                                                ).asInt
                                                            )
                                                        } else {
                                                            runnerModel.layBFVolume.set(
                                                                "" + Math.round(
                                                                    socketAppRateArray[x].asJsonObject.get(
                                                                        JsonKeys.appBFVolume
                                                                    ).asDouble
                                                                )
                                                            )
                                                        }
                                                        isLayDone = true
                                                    } else {
                                                        if (marketModel.MarketType.equals(
                                                                AppConstants.Bookmakers,
                                                                true
                                                            )
                                                        ) {
                                                            runnerModel.backRate.set("-")
                                                            runnerModel.backBFVolume.set("---")
                                                            runnerModel.layRate.set("-")
                                                            runnerModel.layBFVolume.set("---")
                                                        }
                                                    }
                                                }
                                            }
                                            marketModel.Runner[a] = runnerModel
                                        }
                                    }
                                }
                                "9" -> {
                                    marketModel.suspendedText.set(getString(R.string.ball_started))
                                    marketModel.showSuspended.set(true)
                                    for (a in 0 until marketModel.Runner.size) {
                                        marketModel.Runner[a].keyboardOpen.set(false)
                                    }
                                }
                                "4" -> {
                                    marketModel.suspendedText.set(getString(R.string.closed))
                                    marketModel.showSuspended.set(true)
                                    for (a in 0 until marketModel.Runner.size) {
                                        marketModel.Runner[a].keyboardOpen.set(false)
                                    }
//                                    SocketMarketFragment.marketList[j].Market.remove(marketModel)
                                    break
                                }
                                "3" -> {
                                    marketModel.suspendedText.set(getString(R.string.suspended))
                                    marketModel.showSuspended.set(true)
                                    for (a in 0 until marketModel.Runner.size) {
                                        marketModel.Runner[a].keyboardOpen.set(false)
                                    }
                                }
                            }
                        }
                        marketList[j].Market[k] = marketModel
                    }
                }
            }
        } catch (e: Exception) {
//            Config.toast(requireContext(), "" + e)
            Log.e(TAG, "updateSocketData: $e")
        }
    }

    companion object {
        var marketList = ObservableArrayList<MarketDataModel>()
        var socketModel = JsonObject()
        var socketAppRateArray = JsonArray()
        var DownLineData = DownlineModel()
        var isSocketOn = false

        @JvmStatic
        fun newInstance(): SingleMarketFragment {
            return SingleMarketFragment()
        }
    }

    private fun connectWebSocket() {
        try {
            val webUrl: String = "wss://bfrates.com:8888?token=" + "android_betskey_" +
                    Config.getSharedPreferences(requireContext(), PreferenceKeys.Username)
            Log.e(TAG, "socket url: $webUrl")
            val request: Request = Request.Builder().url(webUrl).build()
            SingleMarketActivity.webSocket = SingleMarketActivity.okHttpClient.newWebSocket(
                request,
                object : WebSocketListener() {
                    override fun onOpen(webSocket: WebSocket, response: okhttp3.Response) {
                        val jsonObject = JsonObject()
                        jsonObject.addProperty("action", "set")
                        jsonObject.addProperty("markets", centralIds)
                        Log.e(TAG, "connectWebSocket json: $jsonObject")
                        webSocket.send(jsonObject.toString())
                    }

                    override fun onMessage(webSocket: WebSocket, s: String) {
                        if (SingleMarketActivity.isActive
                        ) {
                            val df: DateFormat = SimpleDateFormat(AppConstants.ddMMMyyyyHHmmssSSS)
                            SingleMarketActivity.socketTime =
                                "Time : " + df.format(Calendar.getInstance().time)
                            val str1: String = s.replace("\\", "")
                            val str2: String = str1.replace("\"[", "[")
                            val str3: String = str2.replace("]\"", "]")
                            val str4: String = str3.replace("}\"", "}")
                            val finalString: String = str4.replace("\"{", "{")
                            val jsonObject = Gson().fromJson(finalString, JsonObject::class.java)
                            parseData(jsonObject)
                        }
                    }

                    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                        webSocket.close(1000, null)

                    }

                    override fun onFailure(
                        webSocket: WebSocket, t: Throwable, response: okhttp3.Response?
                    ) {
                        Log.e(TAG, "onFailure: $t")
                        connectWebSocket()
                    }

                    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                        Log.e(TAG, "onClosed: $reason")
                    }

                })
        } catch (e: Exception) {
            Log.e(TAG, "connectWebSocket: $e")
            return
        }
    }

    private fun parseData(jsonObject: JsonObject) {
        SingleMarketActivity.jsonArray.add(jsonObject)
        Log.e(TAG, "socket data: $jsonObject")
        if (jsonObject.get("data").isJsonObject && jsonObject.get("data").asJsonObject.has("score")) {
            val runnable = Runnable {
                updateSocketData(
                    jsonObject,
                    jsonObject.get("messageType").asString
                )
            }
            requireActivity().runOnUiThread(runnable)
        } else if (jsonObject.get("data").isJsonArray) {
            val data = jsonObject.get("data").asJsonArray[0].asJsonObject
            val runnable = Runnable {
                updateSocketData(
                    data,
                    jsonObject.get("messageType").asString
                )
            }
            requireActivity().runOnUiThread(runnable)
        }
    }
}

@BindingAdapter(value = ["setSingleMarkets"])
fun RecyclerView.setMarkets(markets: List<MarketModel>?) {
    this.run {
        if (markets != null) {
            val marketAdapter = SingleMarketAdapter()
            marketAdapter.submitList(markets)
            adapter = marketAdapter
        }
    }
}

@BindingAdapter(value = ["setSingleRunners"])
fun RecyclerView.setRunners(runners: List<RunnerModel>?) {
    this.run {
        if (runners != null) {
            val runnerAdapter = SingleRunnerAdapter()
            runnerAdapter.submitList(runners)
            this.adapter = runnerAdapter
        }
    }
}
