package com.betskey.dataModels

import android.R

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField

import androidx.databinding.ObservableList
import com.betskey.BR
import com.betskey.databinding.LayoutMarketDataBinding
import java.util.EnumSet.of


class SocketPrefData {
    var BackRate: ObservableField<String> = ObservableField("-")
    var LayRate: ObservableField<String> = ObservableField("-")
    var backBFVolume: ObservableField<String> = ObservableField("---")
    var layBFVolume: ObservableField<String> = ObservableField("---")
    var Name: String = ""
    var RunnerId: Int = 0
    var MarketId: Int = 0
    var keyboardStatus: Boolean = false

//    val items: ObservableList<MarketDataModel> = ObservableArrayList()
//    val itemBinding: LayoutMarketDataBinding<MarketDataModel> = LayoutMarketDataBinding.of
}