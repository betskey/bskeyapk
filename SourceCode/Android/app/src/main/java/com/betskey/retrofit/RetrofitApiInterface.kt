package com.betskey.retrofit

import com.betskey.constants.JsonKeys
import com.betskey.responseModels.Common
import com.betskey.responseModels.DeleteUnMatchedBet
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface RetrofitApiInterface {


    @POST("User/Login")
    fun login(@Header(JsonKeys.X_Signature) key: String, @Body jsonObject: JsonObject): Call<Common>

    @POST("User/Register")
    fun register(
        @Header(JsonKeys.X_Signature) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @GET("User/GetBalanceAndLiability")
    fun getBalanceAndLiability(@Header(JsonKeys.AuthToken) key: String?): Call<Common>

    @GET("Dashboard/GetDashboardDetail")
    fun getDashboardDetail(@Header(JsonKeys.AuthToken) key: String?): Call<Common>

    @PUT("User/Logout")
    fun logout(@Header(JsonKeys.AuthToken) key: String): Call<Common>

    @POST("User/RedictToThirtPartyGame")
    fun redirectToThirdPartyGame(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Binary/Login")
    fun binaryLogin(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Report/GetAccountReport")
    fun getAccountReport(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Report/GetPLReport")
    fun getPLReport(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Report/GetResultsReport")
    fun getResultsReport(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Market/GetMatchedUnMatchedBetDetail")
    fun getMatchedUnMatchedDetail(
        @Header(JsonKeys.AuthToken) key: String?,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Market/GetMarketDetail")
    fun getMarketDetail(
        @Header(JsonKeys.AuthToken) key: String?,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @DELETE
    fun deleteUnmatchedBetById(
        @Header(JsonKeys.AuthToken) key: String?,
        @Url url: String
    ): Call<DeleteUnMatchedBet>

    @PUT("User/UpdateChipsSettings")
    fun updateChipsSettings(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("User/UpdateProfile")
    fun updateProfile(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Market/PlaceBet")
    fun placeBet(
        @Header(JsonKeys.AuthToken) key: String?,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @PUT("User/ChangePassword")
    fun changePassword(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("recharge/PaymentGatwayLogin")
    fun deposit(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("recharge/WithdrawRequest")
    fun withdrawRequest(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @GET("Common/GetAllSports")
    fun getAllSports(@Header(JsonKeys.AuthToken) key: String?): Call<Common>

    @GET
    fun getAllTournaments(@Header(JsonKeys.AuthToken) key: String?, @Url url: String): Call<Common>

    @GET
    fun getAllMatches(@Header(JsonKeys.AuthToken) key: String?, @Url url: String): Call<Common>

    @GET
    fun getAllMarkets(@Header(JsonKeys.AuthToken) key: String?, @Url url: String): Call<Common>

    @POST("Report/GetPLReportHistory")
    fun getPLReportHistory(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Report/GetBetHistoryReport")
    fun getBetHistory(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("Lottery/GetBetList")
    fun getLotteryBetHistory(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>

    @POST("User/GetBetList")
    fun getVeronicaBetHistory(
        @Header(JsonKeys.AuthToken) key: String,
        @Body jsonObject: JsonObject
    ): Call<Common>
}