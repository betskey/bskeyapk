package com.betskey.adapters

import android.annotation.SuppressLint
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.activities.DashboardActivity
import com.betskey.constants.AppConstants
import com.betskey.constants.Config
import com.betskey.dataModels.MarketDataModel
import com.betskey.databinding.LayoutMarketDataBinding
import com.betskey.databinding.LayoutMarketDataSingleBinding
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

class SingleMatchViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    val tvDate: TextView = itemView.findViewById(R.id.tv_date)
    val tvTime: TextView = itemView.findViewById(R.id.tv_time)
    val tvTimer: TextView = itemView.findViewById(R.id.tv_timer)
    val ivPinned: ImageView = itemView.findViewById(R.id.iv_pinned)
}

class SingleMatchAdapter : ListAdapter<MarketDataModel, SingleMatchViewHolder>(Companion) {
    private val viewPool = RecyclerView.RecycledViewPool()
    lateinit var realm: Realm
    val TAG = "SingleMatchAdapter"

    companion object : DiffUtil.ItemCallback<MarketDataModel>() {
        override fun areItemsTheSame(oldItem: MarketDataModel, newItem: MarketDataModel): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: MarketDataModel,
            newItem: MarketDataModel
        ): Boolean {
            return oldItem.MatchId == newItem.MatchId
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingleMatchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutMarketDataSingleBinding.inflate(inflater, parent, false)

        return SingleMatchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SingleMatchViewHolder, position: Int) {
        val currentMatch = getItem(position)
        val itemBinding = holder.binding as LayoutMarketDataSingleBinding
        itemBinding.match = currentMatch
        itemBinding.recyclerMarketList.setRecycledViewPool(viewPool)
        itemBinding.executePendingBindings()
        val localDate = Config.convertIntoLocal(
            Config.stringToDate(currentMatch.OpenDate, AppConstants.yyyyMMddTHHmmSS)!!,
            AppConstants.yyyyMMddTHHmmSS
        )
        val date = SimpleDateFormat(AppConstants.ddMMyyyy).format(
            SimpleDateFormat(AppConstants.yyyyMMddTHHmmSS).parse(localDate)
        )
        val time = SimpleDateFormat(AppConstants.HHmmSSa).format(
            SimpleDateFormat(AppConstants.yyyyMMddTHHmmSS).parse(localDate)
        )
        holder.tvDate.text = date
        holder.tvTime.text = SimpleDateFormat(AppConstants.hhmmSSa).format(
            SimpleDateFormat(AppConstants.yyyyMMddTHHmmSS).parse(localDate)
        )
        val matchDate = Config.stringToDate("$date $time", AppConstants.ddMMyyyyHHmmss)
        val currentDate = Calendar.getInstance().time
        if (matchDate?.compareTo(currentDate) == 1 && !currentMatch.isFancyMarket) {
            val diffTime: Long = matchDate.time - currentDate.time
            holder.tvTimer.visibility = View.VISIBLE
            val timer = object : CountDownTimer(diffTime, 1000) {
                @SuppressLint("SetTextI18n")
                override fun onTick(millisUntilFinished: Long) {
                    val seconds = (millisUntilFinished / 1000) % 60
                    val minutes = ((millisUntilFinished / (1000 * 60)) % 60)
                    val hours = ((millisUntilFinished / (1000 * 60 * 60)) % 24)
                    val days = millisUntilFinished / (24 * 60 * 60 * 1000)
                    holder.tvTimer.text = String.format(
                        "%02d" + "d" + " : %02d" + "h" + " : %02d" + "m" + " : %02d" + "s",
                        days,
                        hours,
                        minutes,
                        seconds
                    )
                }

                override fun onFinish() {
                    holder.tvTimer.visibility = View.GONE
                }
            }
            timer.start()
        } else
            holder.tvTimer.visibility = View.GONE

        holder.ivPinned.visibility = View.GONE
        /*holder.ivPinned.setOnClickListener {
            mClickListener?.onItemClick(holder.ivPinned, position)
        }*/
    }

    var mClickListener: ItemClickListener? = null

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}