package com.betskey.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.betskey.constants.Config
import com.betskey.constants.PreferenceKeys
import com.betskey.databinding.ActivitySplashBinding


class SplashActivity : AppCompatActivity() {

    var TAG: String = "SplashActivity"
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val handler = Handler()
        val runnable = Runnable { init() }
        handler.postDelayed(runnable, 3000)
    }

    private fun init() {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
            binding.ivAppLogo, "splash_transition")
        if (Config.getUserLoggedIn(this, PreferenceKeys.userLoggedIn)) {
            startActivity(Intent(this@SplashActivity, DashboardActivity::class.java), options.toBundle())
            finish()
        } else {
            startActivity(Intent(this@SplashActivity, LanguageSelectionActivity::class.java), options.toBundle())
            finishAffinity()
        }
    }
}