package com.betskey.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.dataModels.HighlightDataModel
import com.betskey.responseModels.Match
import io.realm.Realm

class InPlayListAdapter(private val items: ArrayList<HighlightDataModel>?, val context: Context) :
    RecyclerView.Adapter<InPlayListAdapter.MyViewHolder>(){

    val TAG: String = "InPlayListAdapter"
    lateinit var adapter: MatchListAdapter
    lateinit var dataModel: HighlightDataModel
    lateinit var realm: Realm

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.in_play_row_layout, parent, false)
        return MyViewHolder(view)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llSportName: LinearLayout = itemView.findViewById(R.id.ll_sport_name)
        val tvName: TextView = itemView.findViewById(R.id.tv_sport_name)
        val ivImage: ImageView = itemView.findViewById(R.id.iv_sport_icon)
        val recyclerView: RecyclerView = itemView.findViewById(R.id.recycler_in_play_list)
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        realm = Realm.getDefaultInstance()
        dataModel = items!![position]
        holder.tvName.setText(dataModel.SportName)
        val id: Int = context?.resources!!
            .getIdentifier(
                dataModel.SportName.toLowerCase(),
                "drawable",
                context.packageName
            )
        holder.ivImage.setImageResource(id)

        if (dataModel.Match.size > 0) {
            holder.llSportName.visibility = View.VISIBLE
            holder.recyclerView.layoutManager =
                LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = MatchListAdapter(dataModel.Match, context)
            holder.recyclerView.adapter = adapter
        } else {
            holder.llSportName.visibility = View.GONE
        }
    }
}