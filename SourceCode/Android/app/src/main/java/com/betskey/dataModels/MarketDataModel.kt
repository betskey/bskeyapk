package com.betskey.dataModels

import androidx.databinding.ObservableField

class MarketDataModel {
    var MatchId: Int = 0
    var TournamentId: Int = 0
    var BfMatchId: Int = 0
    var MatchName: String = ""
    var MatchDescription: String = ""
    var OpenDate: String = ""
    var StreamingUrl: String = ""
    var Market: ArrayList<MarketModel> = ArrayList()
    var isFancyMarket: Boolean = false
    var inPlay : ObservableField<Boolean> = ObservableField(false)
    var inningType : ObservableField<String> = ObservableField("")
    var battingTeam : ObservableField<String> = ObservableField("")
    var bowlingTeam : ObservableField<String> = ObservableField("")
    var runWicketOverBatT : ObservableField<String> = ObservableField("")
    var runWicketOverBowlT : ObservableField<String> = ObservableField("")
    var tossStatus : ObservableField<String> = ObservableField("")
}