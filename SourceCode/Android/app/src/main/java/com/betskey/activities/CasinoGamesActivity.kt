package com.betskey.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.betskey.R
import com.betskey.constants.Config
import com.betskey.constants.IntentKeys
import com.betskey.constants.JsonKeys
import com.betskey.constants.PreferenceKeys
import com.betskey.databinding.ActivityCasinoGamesBinding
import com.betskey.responseModels.CasinoGames
import com.betskey.responseModels.Common
import com.betskey.retrofit.RetrofitApiClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CasinoGamesActivity : AppCompatActivity() {

    val TAG: String = "CasinoGamesActivity"
    lateinit var binding: ActivityCasinoGamesBinding
    lateinit var mWebView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCasinoGamesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun init() {
        binding.ivBack.setOnClickListener { finish() }
        mWebView = binding.webview
        val casinoModel: CasinoGames =
            intent.getSerializableExtra(IntentKeys.casinoGame) as CasinoGames
        binding.tvTitle.text = casinoModel.Name
        callApi(casinoModel)
        binding.btnReloadData.setOnClickListener { callApi(casinoModel) }
    }

    private fun callApi(casinoModel: CasinoGames) {
        casinoModel.GameType = "Veronica"
        val jsonObject = JsonObject()
        jsonObject.addProperty(JsonKeys.GameType, casinoModel.GameType)
        if (!casinoModel.GameCode.isNullOrEmpty()) {
            jsonObject.addProperty(JsonKeys.GameCode, casinoModel.GameCode)
        } else {
            jsonObject.addProperty(JsonKeys.GameCode, "")
        }
        jsonObject.addProperty(
            JsonKeys.ClientUserName,
            Config.getSharedPreferences(this, PreferenceKeys.Username)
        )
        Log.e(TAG, "redirectToThirdPartGame: $jsonObject")
        try {
            if (Config.isInternetAvailable(this)) {
                binding.webview.visibility = View.VISIBLE
                binding.llNoInternet.visibility = View.GONE
                Config.showSmallProgressDialog(this)
                val call: Call<Common> = when (casinoModel.GameType) {
                    "Binary" -> RetrofitApiClient.getClient.binaryLogin(
                        Config.getSharedPreferences(this, PreferenceKeys.AuthToken)!!,
                        jsonObject
                    )
                    else -> RetrofitApiClient.getClient.redirectToThirdPartyGame(
                        Config.getSharedPreferences(this, PreferenceKeys.AuthToken)!!,
                        jsonObject
                    )
                }
                call.enqueue(object : Callback<Common> {
                    @SuppressLint("SetJavaScriptEnabled")
                    override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                        val common: Common? = response?.body()
                        Log.e(TAG, "Game response: " + Gson().toJson(common))
                        if (response != null && response.isSuccessful) {
                            if (common?.status?.code == 0) {
                                if (!common.RedirectUrl.isNullOrEmpty()) {
                                    mWebView.settings.javaScriptEnabled = true
                                    mWebView.webViewClient = WebViewClient()
                                    mWebView.settings.domStorageEnabled = true
                                    mWebView.webViewClient = object : WebViewClient() {
                                        override fun onPageFinished(view: WebView, url: String) {
                                            Config.hideSmallProgressDialog()
                                        }
                                    }
                                    mWebView.loadUrl(common.RedirectUrl)
                                } else {
                                    Config.hideSmallProgressDialog()
                                    Config.showOkDialog(
                                        this@CasinoGamesActivity,
                                        getString(R.string.cannot_open_this_game)
                                    )
                                }
                            } else {
                                Config.hideSmallProgressDialog()
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction { realm -> realm.deleteAll() }
                                Config.clearAllPreferences(this@CasinoGamesActivity)
                                startActivity(
                                    Intent(
                                        this@CasinoGamesActivity,
                                        LoginActivity::class.java
                                    )
                                )
                                finish()
                            }
                        } else {
                            Config.hideSmallProgressDialog()
                            val realm = Realm.getDefaultInstance()
                            realm.executeTransaction { realm -> realm.deleteAll() }
                            Config.clearAllPreferences(this@CasinoGamesActivity)
                            startActivity(
                                Intent(
                                    this@CasinoGamesActivity,
                                    LoginActivity::class.java
                                )
                            )
                            finish()
                        }
                    }

                    override fun onFailure(call: Call<Common>?, t: Throwable?) {
                        Config.hideSmallProgressDialog()
                        Log.e(TAG, "redirectToThirdPartGame: " + t.toString())
                    }
                })
            } else {
                Config.hideSmallProgressDialog()
                binding.webview.visibility = View.GONE
                binding.llNoInternet.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            Config.toast(this, "" + e)
            Log.e(TAG, "" + e)
        }
    }
}