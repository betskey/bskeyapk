package com.betskey.dataModels

import java.io.Serializable

data class UnMatchedBetModel(
    var name: String? = "",
    var id: Int = 0
) : Serializable