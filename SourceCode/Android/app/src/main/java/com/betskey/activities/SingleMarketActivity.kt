package com.betskey.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.betskey.R
import com.betskey.adapters.MarketViewPagerAdapter
import com.betskey.constants.*
import com.betskey.dataModels.MatchedBetModel
import com.betskey.databinding.ActivitySingleMarketBinding
import com.betskey.fragments.singleMarketFragments.SingleMarketFragment
import com.betskey.fragments.singleMarketFragments.SingleMatchedFragment
import com.betskey.fragments.singleMarketFragments.SingleUnmatchedFragment
import com.betskey.responseModels.Common
import com.betskey.retrofit.RetrofitApiClient
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.Realm
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SingleMarketActivity : AppCompatActivity() {

    val TAG: String = "SingleMarketActivity"
    lateinit var binding: ActivitySingleMarketBinding
    lateinit var realm: Realm
    var centralIds: String = ""
    var socketMarketFragment = SingleMarketFragment()
    var matchedFragment = SingleMatchedFragment()
    var unMatchedFragment = SingleUnmatchedFragment()
    var centralIDList = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySingleMarketBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    companion object {
        var okHttpClient: OkHttpClient = OkHttpClient()
        lateinit var webSocket: WebSocket
        var socketTime: String = ""
        var matchedList = ArrayList<MatchedBetModel>()
        var unMatchedList = ArrayList<MatchedBetModel>()
        var jsonArray = JsonArray()
        lateinit var viewPagerAdapter: MarketViewPagerAdapter
        var marketIds: String = ""
        lateinit var tvBalance: TextView
        lateinit var tvLiability: TextView
        lateinit var swipeRefreshLayout: SwipeRefreshLayout
        var isActive = false
    }

    private fun init() {
        tvBalance = binding.tvBalanceAcc
        tvLiability = binding.tvLiabilityAcc
        swipeRefreshLayout = binding.refresh
        swipeRefreshLayout.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                this,
                R.color.bgColor
            )
        )
        swipeRefreshLayout.setColorSchemeColors(Color.WHITE)
        if (intent.hasExtra(IntentKeys.matchName) && intent.getStringExtra(IntentKeys.matchName) != null) {
            binding.tvTitle.text = intent.getStringExtra(IntentKeys.matchName)!!
        }
        setBalanceAndLiability()

        binding.btnReloadData.setOnClickListener {
            if (isActive) {
                webSocket.close(1000, null)
                isActive = false
            }
            setBalanceAndLiability()
            loadData()
        }
        binding.ivBack.setOnClickListener { onBackPressed() }
        swipeRefreshLayout.setOnRefreshListener {
            if (isActive) {
                webSocket.close(1000, null)
                isActive = false
            }
            setBalanceAndLiability()
            loadData()
            swipeRefreshLayout.isRefreshing = false
        }
        swipeRefreshLayout.isEnabled = false
    }

    override fun onResume() {
        super.onResume()
        marketIds = ""
        if (isActive) {
            webSocket.close(1000, null)
            isActive = false
        }
        if (AppConstants.matchClickedMarket) {
            marketIds = Config.getSharedPreferences(this, PreferenceKeys.matchMarketList)!!
        } else if (AppConstants.liabilityClickedMarket) {
            marketIds = Config.getSharedPreferences(this, PreferenceKeys.liabilityMarketList)!!
        }
        loadData()
    }

    private fun setBalanceAndLiability() {
        binding.tvBalanceAcc.text = Config.getSharedPreferences(this, PreferenceKeys.balance)
        binding.tvLiabilityAcc.text = Config.getSharedPreferences(this, PreferenceKeys.liability)
        binding.tvLiabilityAcc.setOnClickListener {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                binding.tvLiabilityAcc, "liability_transition"
            )
            val intent = Intent(this, LiabilityDetailActivity::class.java)
            intent.putExtra(IntentKeys.liabilityData, DashboardActivity.liabilityList)
            intent.putExtra(IntentKeys.data, marketIds)
            startActivity(intent, options.toBundle())
        }
    }

    private fun loadData() {
        if (Config.isInternetAvailable(this)) {
            binding.llData.visibility = View.VISIBLE
            binding.llNoInternet.visibility = View.GONE
//            callMarketDetail()
            callMatchedUnMatchedDetail()
            setupViewPager()
        } else {
            binding.llData.visibility = View.GONE
            binding.llNoInternet.visibility = View.VISIBLE
        }
    }

    private fun callMarketDetail() {
        val jsonObject = JsonObject()
        jsonObject.addProperty(JsonKeys.MarketIds, marketIds)
        if (Config.isInternetAvailable(this)) {
            Config.showSmallProgressDialog(this)
            val call: Call<Common> = RetrofitApiClient.getClient.getMarketDetail(
                Config.getSharedPreferences(this, PreferenceKeys.AuthToken), jsonObject
            )
            call.enqueue(object : Callback<Common> {
                override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                    val common: Common? = response?.body()
                    if (response != null && response.isSuccessful) {
                        when (common?.status?.code) {
                            0 -> {
                                centralIds = ""
                                centralIDList.clear()
                                for (i in 0 until common.MarketData.size) {
                                    for (j in 0 until common.MarketData[i].Market.size) {
                                        centralIDList.add(common.MarketData[i].Market[j].CentralId)
                                    }
                                }
                                for (i in 0 until common.FancyMarketData.size) {
                                    for (j in 0 until common.FancyMarketData[i].Market.size) {
                                        centralIDList.add(common.FancyMarketData[i].Market[j].CentralId)
                                    }
                                }
                                centralIds = TextUtils.join(",", centralIDList)
                                if (centralIds.isNotEmpty()) {
                                    isActive = true
                                    connectWebSocket()
                                }
                                Config.hidePlaceBetProgress()
                            }
                            401 -> {
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction { realm -> realm.deleteAll() }
                                Config.clearAllPreferences(this@SingleMarketActivity)
                                startActivity(
                                    Intent(
                                        this@SingleMarketActivity,
                                        LoginActivity::class.java
                                    )
                                )
                                finish()
                            }
                            else -> {
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction { realm -> realm.deleteAll() }
                                Config.clearAllPreferences(this@SingleMarketActivity)
                                startActivity(
                                    Intent(
                                        this@SingleMarketActivity,
                                        LoginActivity::class.java
                                    )
                                )
                                finish()
                            }
                        }
                    } else {
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction { realm -> realm.deleteAll() }
                        Config.clearAllPreferences(this@SingleMarketActivity)
                        startActivity(Intent(this@SingleMarketActivity, LoginActivity::class.java))
                        finish()
                    }
                }

                override fun onFailure(call: Call<Common>?, t: Throwable?) {
                    Log.e(TAG, "getMarketDetail: " + t.toString())
                }
            })
        } else {
            binding.llData.visibility = View.VISIBLE
            binding.llNoInternet.visibility = View.GONE
        }
    }

    private fun callMatchedUnMatchedDetail() {
        val jsonObject = JsonObject()
        jsonObject.addProperty(JsonKeys.MarketIds, marketIds)
        try {
            Config.showSmallProgressDialog(this)
            val call: Call<Common> =
                RetrofitApiClient.getMarketApiClient.getMatchedUnMatchedDetail(
                    Config.getSharedPreferences(this, PreferenceKeys.AuthToken),
                    jsonObject
                )
            call.enqueue(object : Callback<Common> {
                override fun onResponse(call: Call<Common>?, response: Response<Common>?) {
                    val common: Common? = response?.body()
                    Log.e(TAG, "getMatchedUnMatchedDetail: " + Gson().toJson(response?.body()))
                    Config.hideSmallProgressDialog()
                    if (response != null) {
                        if (common?.status?.code == 0) {
                            matchedList = common.MatchedBetData
                            unMatchedList = common.UnMatchedBetData
                        } else if (common?.status?.code == 401) {
                            val realm = Realm.getDefaultInstance()
                            realm.executeTransaction { realm -> realm.deleteAll() }
                            Config.clearAllPreferences(this@SingleMarketActivity)
                            startActivity(
                                Intent(
                                    this@SingleMarketActivity,
                                    LoginActivity::class.java
                                )
                            )
                            finish()
                        }
                    } else {
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction { realm -> realm.deleteAll() }
                        Config.clearAllPreferences(this@SingleMarketActivity)
                        startActivity(Intent(this@SingleMarketActivity, LoginActivity::class.java))
                        finish()
                    }
                }

                override fun onFailure(call: Call<Common>?, t: Throwable?) {
                    Config.hideSmallProgressDialog()
                    Log.e(TAG, "getMatchedUnMatchedDetail: " + t.toString())
                }
            })
        } catch (e: Exception) {
            Config.toast(this, "" + e)
            Log.e(TAG, "" + e)
        }
    }

    private fun connectWebSocket() {
        try {
            val webUrl: String = "wss://bfrates.com:8888?token=" + "android_betskey_" +
                    Config.getSharedPreferences(this, PreferenceKeys.Username)
            Log.e(TAG, "socket url: $webUrl")
            val request: Request = Request.Builder().url(webUrl).build()
            webSocket = okHttpClient.newWebSocket(
                request,
                object : WebSocketListener() {
                    override fun onOpen(webSocket: WebSocket, response: okhttp3.Response) {
                        val jsonObject = JsonObject()
                        jsonObject.addProperty("action", "set")
                        jsonObject.addProperty("markets", centralIds)
                        Log.e(TAG, "connectWebSocket json: $jsonObject")
                        webSocket.send(jsonObject.toString())
                    }

                    override fun onMessage(webSocket: WebSocket, s: String) {
                        if (isActive) {
                            val df: DateFormat = SimpleDateFormat(AppConstants.ddMMMyyyyHHmmssSSS)
                            socketTime = "Time : " + df.format(Calendar.getInstance().time)
                            val str1: String = s.replace("\\", "")
                            val str2: String = str1.replace("\"[", "[")
                            val str3: String = str2.replace("]\"", "]")
                            val str4: String = str3.replace("}\"", "}")
                            val finalString: String = str4.replace("\"{", "{")
                            val jsonObject = Gson().fromJson(finalString, JsonObject::class.java)
                            parseData(jsonObject)
                        }
                    }

                    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                        webSocket.close(1000, null)
                    }

                    override fun onFailure(
                        webSocket: WebSocket, t: Throwable, response: okhttp3.Response?
                    ) {
                        Log.e(TAG, "onFailure: $t")
                        connectWebSocket()
                    }

                    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                        Log.e(TAG, "onClosed: $reason")
                    }

                })
        } catch (e: Exception) {
            Log.e(TAG, "connectWebSocket: $e")
            return
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun parseData(jsonObject: JsonObject) {
        jsonArray.add(jsonObject)
//        Log.e(TAG, "socket data: $jsonObject")
        if (jsonObject.get("data").isJsonObject && jsonObject.get("data").asJsonObject.has("score")) {
            val runnable = Runnable {
                socketMarketFragment.updateSocketData(
                    jsonObject,
                    jsonObject.get("messageType").asString
                )
            }
            runOnUiThread(runnable)
        } else if (jsonObject.get("data").isJsonArray) {
            val data = jsonObject.get("data").asJsonArray[0].asJsonObject
            val runnable = Runnable {
                socketMarketFragment.updateSocketData(
                    data,
                    jsonObject.get("messageType").asString
                )
            }
            runOnUiThread(runnable)
        }
    }

    private fun setupViewPager() {
        viewPagerAdapter = MarketViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(
            socketMarketFragment, getString(R.string.markets)
        )
        viewPagerAdapter.addFragment(
            matchedFragment,
            getString(R.string.matched)
        )
        viewPagerAdapter.addFragment(
            unMatchedFragment,
            getString(R.string.unmatched)
        )
        binding.viewpager.offscreenPageLimit = 2
        binding.viewpager.adapter = viewPagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewpager, false)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isActive) {
            webSocket.close(1000, null)
            isActive = false
        }
        finish()
    }
}