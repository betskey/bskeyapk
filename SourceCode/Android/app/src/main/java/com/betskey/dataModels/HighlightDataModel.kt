package com.betskey.dataModels


import com.betskey.responseModels.Match

class HighlightDataModel {
    var SportId: Int = 0
    var SportName: String = ""
    var Icon: String = ""
    var Match: ArrayList<Match> = ArrayList()
}