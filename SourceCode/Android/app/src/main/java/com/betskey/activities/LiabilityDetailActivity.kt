package com.betskey.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.betskey.R
import com.betskey.adapters.LiabilityListAdapter
import com.betskey.constants.AppConstants
import com.betskey.constants.Config
import com.betskey.constants.IntentKeys
import com.betskey.constants.PreferenceKeys
import com.betskey.dataModels.LiabilityDataModel
import com.betskey.databinding.ActivityLiabilityDetailBinding


class LiabilityDetailActivity : AppCompatActivity(), LiabilityListAdapter.ItemClickListener {

    var TAG: String = "LiabilityDetailActivity"
    private lateinit var binding: ActivityLiabilityDetailBinding
    var data = ArrayList<LiabilityDataModel>()
    lateinit var adapter: LiabilityListAdapter
    var marketIDs = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLiabilityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.refresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                this,
                R.color.bgColor
            )
        )
        binding.refresh.setColorSchemeColors(Color.WHITE)
        binding.ivBack.setOnClickListener { finishAfterTransition() }
        if (intent.hasExtra(IntentKeys.data))
            marketIDs = intent.getStringExtra(IntentKeys.data)!!
        data =
            intent.getSerializableExtra(IntentKeys.liabilityData) as ArrayList<LiabilityDataModel>
        binding.recyclerLiabilityData.layoutManager = LinearLayoutManager(
            this, RecyclerView.VERTICAL, false
        )
        adapter = LiabilityListAdapter(data, this)
        binding.recyclerLiabilityData.adapter = adapter
        adapter.setClickListener(this)

        var liability = 0.0
        if (data.size > 0) {
            for (i in 0 until data.size) {
                liability += data[i].Liability
            }
        }
        binding.tvTotalLiability.text = DashboardActivity.decimalFormat0_00.format(
            liability.toBigDecimal()
        ).toString()

        binding.refresh.setOnRefreshListener {
            binding.refresh.isRefreshing = false
        }
    }

    override fun onItemClick(view: View?, position: Int) {
        val model = data[position]
        AppConstants.matchClickedMarket = false
        AppConstants.liabilityClickedMarket = true
        Config.saveSharedPreferences(this, PreferenceKeys.liabilityMarketList, model.MarketId)
//        val intent = Intent(this, SingleMarketActivity::class.java)
//        intent.putExtra(IntentKeys.marketID, model.MarketId)
//        startActivity(intent)
        startActivity(Intent(this, SingleMarketActivity::class.java))
    }

    override fun onBackPressed() {
        AppConstants.matchClickedMarket = true
        AppConstants.liabilityClickedMarket = false
        super.onBackPressed()
        finishAfterTransition()
    }
}